# CSGO-smokes

This is a project / collection of repositories for CSGO smokes for different maps.

## Naming convention

On the off chance that someone stumbles upon this project and wants to contribute, please respect these naming conventions:

* Absolutely NO SPACES in file names - use underscores
* Only lowercase letters, this also includes file extensions
* Only *.jpg and *.png
* Number each screenshot according to order of execution. eg:
	* long_from_second_mid_01.jpg
	* long_from_second_mid_02.jpg
	* long_from_second_mid_03.jpg
	* long_from_second_mid_04_result.jpg
		* *_result.jpg is optional
* Technically not a naming convention, but please take screenshots in a reasonable resolution (1080p is preferred)

## Scripts

### imgtomd.py

File name is pretty self-explanatory. It checks the current directory for *.jpg and *.png, adds markdown syntax to each filename and writes it to a markdown file with proper line breaks.

### Taking screenshots

For now, screenshots are taken and renamed by hand. I personally prefer to use ShareX, since it can automatically save to file without much configuration.


