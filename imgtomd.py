import glob

def img_append():
    for file in files:
        img = f"![{file}]({file})\n\n"
        output_array.append(img)

files = glob.glob("*.jpg")
files.sort()

output_array = list()

img_append()

files = glob.glob("*.png")
files.sort()

img_append()

output = ''.join(output_array)

with open("output.md", "w") as f:
    f.write(output)

